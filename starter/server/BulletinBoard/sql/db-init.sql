-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic');

-- insert authorities
insert into security_authority (name) values ('ROLE_ADMIN'); -- super user
insert into security_authority (name) values ('ROLE_USER'); -- normal user

-- insert mappings between users and authorities
insert into security_user_authority (user_id, authority_id) values (1, 1); -- admin has ROLE_ADMIN
insert into security_user_authority (user_id, authority_id) values (1, 2); -- admin has ROLE_USER too
insert into security_user_authority (user_id, authority_id) values (2, 2); -- petar has ROLE_USER

insert into category (id, name) values (1, "Obavestenje");
insert into category (id, name) values (2, "Upozorenje");
insert into category (id, name) values (3, "Kvarovi");

insert into post (id, date, text, category_id, creator_id) values (1, "2019-05-13", "obavestenje", 1, 1);
insert into post (id, date, text, category_id, creator_id) values (2, "2019-05-14", "upozorenje", 2, 2);
insert into post (id, date, text, category_id, creator_id) values (3, "2019-05-15", "kvarovi", 3, 2);
insert into post (id, date, text, category_id, creator_id) values (4, "2019-05-15", "kvarovi", 3, 2);
insert into post (id, date, text, category_id, creator_id) values (5, "2019-05-14", "upozorenje", 2, 2);
insert into post (id, date, text, category_id, creator_id) values (6, "2019-05-13", "obavestenje", 1, 1);


insert into comment (id, date , text, creator_id, post_id) values (1, "2019-05-14", "komentar 1", 1, 1);
insert into comment (id, date , text, creator_id, post_id) values (2, "2019-05-15", "komentar 2", 2, 1);

insert into reply (id, text, creator_id, comment_id) values (1, "reply 1", 2, 1);
insert into reply (id, text, creator_id, comment_id) values (2, "reply 2", 1, 1);

insert into dm (id, creator, date, text, receiver_id) values (1, "petar", "2019-05-13", "bla", 1);
insert into dm (id, creator, date, text, receiver_id) values (2, "admin", "2019-05-13", "bla", 2);







