package vp.spring.rcs.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.user.SecurityAuthority;


public interface SecurityAuthorityRepository
	extends JpaRepository<SecurityAuthority, Long> {

	List<SecurityAuthority> findByNameIn(List<String> names);
	
}
