package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Grupa;

public interface GroupRepository extends JpaRepository <Grupa, Long> {

	public Grupa findByNameLike (String name);
}
