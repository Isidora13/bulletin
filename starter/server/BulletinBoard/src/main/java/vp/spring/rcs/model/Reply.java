package vp.spring.rcs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Reply {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	SecurityUser creator;

	@Column(length = 500)
	private String text;

	@ManyToOne(fetch = FetchType.EAGER)
	Comment comment;

	public Reply() {
		super();
	}

	public Reply(Long id, SecurityUser creator, String text, Comment comment) {
		super();
		this.id = id;
		this.creator = creator;
		this.text = text;
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SecurityUser getCreator() {
		return creator;
	}

	public void setCreator(SecurityUser creator) {
		this.creator = creator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@JsonIgnore
	public Comment getComment() {
		return comment;
	}
	@JsonIgnore
	public void setComment(Comment comment) {
		this.comment = comment;
	}

}
