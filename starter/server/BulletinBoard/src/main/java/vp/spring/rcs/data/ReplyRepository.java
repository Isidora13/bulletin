package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Reply;

public interface ReplyRepository extends JpaRepository <Reply, Long> {

}
