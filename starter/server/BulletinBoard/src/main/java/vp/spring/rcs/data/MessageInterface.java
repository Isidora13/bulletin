package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Message;

public interface MessageInterface  extends JpaRepository <Message, Long> {

}
