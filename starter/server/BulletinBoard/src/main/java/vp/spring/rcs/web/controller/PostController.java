package vp.spring.rcs.web.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.data.CategoryRepository;
import vp.spring.rcs.data.CommentRepository;
import vp.spring.rcs.data.DMRepository;
import vp.spring.rcs.data.GroupRepository;
import vp.spring.rcs.data.MessageInterface;
import vp.spring.rcs.data.PostRepository;
import vp.spring.rcs.data.ReplyRepository;
import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.Category;
import vp.spring.rcs.model.Comment;
import vp.spring.rcs.model.DM;
import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.Message;
import vp.spring.rcs.model.Post;
import vp.spring.rcs.model.Reply;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.web.dto.MessageDTO;

@RestController
public class PostController {

	@Autowired
	PostRepository postRepository;

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	GroupRepository groupRepository;

	@Autowired
	ReplyRepository replyRepository;

	@Autowired
	MessageInterface messageRepository;

	@Autowired
	DMRepository dmRepository;

	@GetMapping(value = "/api/posts")
	public ResponseEntity<List<Post>> getAll() {
		List<Post> posts = postRepository.findAll();

		return new ResponseEntity<>(posts, HttpStatus.OK);
	}

	@PostMapping(value = "/api/posts")
	public ResponseEntity<Post> addPost(@RequestBody Post post) {
		Post postDTO = new Post();
		SecurityUser creator = userRepository.findByUsername(post.getCreator().getUsername());
		postDTO.setCreator(creator);
		Category category = categoryRepository.findByNameLike(post.getCategory().getName());
		postDTO.setCategory(category);
		postDTO.setText(post.getText());
		postDTO.setTitle(post.getTitle());
		postDTO.setDate(post.getDate());
		postRepository.save(postDTO);

		return new ResponseEntity<>(postDTO, HttpStatus.CREATED);
	}

	@GetMapping(value = "/api/posts/{id}")
	public ResponseEntity<Post> getOne(@PathVariable Long id) {
		Post post = postRepository.findOne(id);
		return new ResponseEntity<>(post, HttpStatus.OK);
	}

	@GetMapping(value = "/api/categories")
	public ResponseEntity<List<Category>> getCategories() {
		List<Category> categories = categoryRepository.findAll();
		return new ResponseEntity<>(categories, HttpStatus.OK);
	}

	@GetMapping(value = "/api/posts/{id}/comments")
	public ResponseEntity<Set<Comment>> getComments(@PathVariable Long id) {
		Post post = postRepository.findOne(id);
		Set<Comment> comments = post.getComments();

		return new ResponseEntity<>(comments, HttpStatus.OK);
	}

	@PostMapping(value = "/api/posts/{id}/comments")
	public ResponseEntity<Comment> newComment(@PathVariable Long id, @RequestBody Comment comment) {
		Post post = postRepository.findOne(id);
		Comment commentDTO = new Comment();
		commentDTO.setPost(post);
		String username = comment.getCreator().getUsername();
		commentDTO.setCreator(userRepository.findByUsername(username));
		commentDTO.setText(comment.getText());
		commentDTO.setDate(comment.getDate());

		commentRepository.save(commentDTO);
		return new ResponseEntity<>(commentDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/posts/list", method = RequestMethod.GET)
	public ResponseEntity<Page<Post>> getAllPage(Pageable page) {

		Page<Post> postPage = postRepository.findAll(page);

		List<Post> posts = postPage.getContent();

		Page<Post> retVal = new PageImpl<>(posts, page, postPage.getTotalElements());

		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/posts/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		postRepository.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/api/group")
	public ResponseEntity<Grupa> createGroup(@RequestBody Grupa grupa) {
		Set<Grupa> grupe = new HashSet<>();
		Grupa newGrupa = new Grupa();
		newGrupa.setName(grupa.getName());
		grupe.add(newGrupa);
		for (SecurityUser u : grupa.getCreators()) {
			SecurityUser user = userRepository.findByUsername(u.getUsername());
			user.getGrupe().addAll(grupe);
		}
		groupRepository.save(newGrupa);

		/*
		 * for (SecurityUser u: newGrupa.getCreators()) { u.setGrupe(grupe);
		 * userRepository.save(u); } groupRepository.save(grupe);
		 */

		return new ResponseEntity<>(newGrupa, HttpStatus.CREATED);
	}

	@GetMapping(value = "/api/groups")
	public ResponseEntity<List<Grupa>> getAllGrupe() {
		List<Grupa> grupe = groupRepository.findAll();

		return new ResponseEntity<>(grupe, HttpStatus.OK);
	}

	@GetMapping(value = "/api/group")
	public ResponseEntity<Grupa> getGrupa(@RequestParam String name) {
		Grupa grupa = groupRepository.findByNameLike(name);
		return new ResponseEntity<>(grupa, HttpStatus.OK);
	}

	@PostMapping(value = "/api/reply")
	public ResponseEntity<Reply> newReply(@RequestBody Reply reply, @RequestParam Long commentId) {
		Comment c = commentRepository.findOne(commentId);
		Reply replyDTO = new Reply();
		replyDTO.setComment(c);
		String username = reply.getCreator().getUsername();
		replyDTO.setCreator(userRepository.findByUsername(username));
		replyDTO.setText(reply.getText());

		replyRepository.save(replyDTO);
		return new ResponseEntity<>(replyDTO, HttpStatus.OK);
	}

	@PostMapping(value = "/api/group/message")
	public ResponseEntity<MessageDTO> newMessage(@RequestBody MessageDTO message) {
		Message newMessage = new Message();
		Grupa grupa = groupRepository.findByNameLike(message.getGrupa().getName());
		newMessage.setGrupa(grupa);
		String username = message.getCreator().getUsername();
		newMessage.setCreator(userRepository.findByUsername(username));
		newMessage.setText(message.getText());
		newMessage.setDate(message.getDate());

		messageRepository.save(newMessage);
		MessageDTO converted = new MessageDTO(newMessage);
		return new ResponseEntity<>(converted, HttpStatus.CREATED);
	}

	@PostMapping(value = "/api/dm")
	public ResponseEntity<DM> newDm(@RequestBody DM dm, @RequestParam String username) {
		SecurityUser user = userRepository.findByUsername(username);
		DM newDm = new DM();
		newDm.setReceiver(user);
		newDm.setCreator(dm.getCreator());
		newDm.setDate(dm.getDate());
		newDm.setText(dm.getText());

		dmRepository.save(newDm);

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
