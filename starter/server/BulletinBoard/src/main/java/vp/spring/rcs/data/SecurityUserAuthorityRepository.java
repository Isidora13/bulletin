package vp.spring.rcs.data;


import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.user.SecurityUserAuthority;


public interface SecurityUserAuthorityRepository
	extends JpaRepository<SecurityUserAuthority, Long> {


}
