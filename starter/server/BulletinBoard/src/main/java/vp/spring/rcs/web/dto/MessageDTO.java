package vp.spring.rcs.web.dto;

import java.util.Date;

import vp.spring.rcs.model.Message;
import vp.spring.rcs.model.user.SecurityUser;

public class MessageDTO {

	private Long id;
	private Date date;

	private String text;
	private SecurityUser creator;

	private GrupaDTO grupa;

	public MessageDTO() {
		super();
	}

	public MessageDTO(Long id, Date date, String text, SecurityUser creator, GrupaDTO grupa) {
		super();
		this.id = id;
		this.date = date;
		this.text = text;
		this.creator = creator;
		this.grupa = grupa;
	}

	public MessageDTO(Message message) {
		this.id = message.getId();
		this.date = message.getDate();
		this.text = message.getText();
		this.creator = message.getCreator();
		this.grupa = new GrupaDTO(message.getGrupa());

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public SecurityUser getCreator() {
		return creator;
	}

	public void setCreator(SecurityUser creator) {
		this.creator = creator;
	}

	public GrupaDTO getGrupa() {
		return grupa;
	}

	public void setGrupa(GrupaDTO grupa) {
		this.grupa = grupa;
	}

}
