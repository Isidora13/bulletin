package vp.spring.rcs.model.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.Comment;
import vp.spring.rcs.model.DM;
import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.Message;
import vp.spring.rcs.model.Post;
import vp.spring.rcs.model.Reply;

@Entity
public class SecurityUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String username;

	private String password;

	private String firstName;

	private String lastName;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<SecurityUserAuthority> userAuthorities = new HashSet<SecurityUserAuthority>();

	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Comment> comments = new HashSet<Comment>();

	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Reply> replies = new HashSet<Reply>();

	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Post> posts = new HashSet<Post>();

	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Message> messages = new HashSet<Message>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	private Set<Grupa> grupe = new HashSet<>();

	@OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<DM> dms = new HashSet<DM>();

	public SecurityUser() {
		super();
	}

	public SecurityUser(Long id, String username, String password, String firstName, String lastName,
			Set<SecurityUserAuthority> userAuthorities, Set<Comment> comments, Set<Reply> replies, Set<Post> posts,
			Set<Message> messages, Set<Grupa> grupe, Set<DM> dms) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userAuthorities = userAuthorities;
		this.comments = comments;
		this.replies = replies;
		this.posts = posts;
		this.messages = messages;
		this.grupe = grupe;
		this.dms = dms;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonIgnore
	public Set<SecurityUserAuthority> getUserAuthorities() {
		return userAuthorities;
	}

	@JsonIgnore
	public void setUserAuthorities(Set<SecurityUserAuthority> userAuthorities) {
		this.userAuthorities = userAuthorities;
	}

	@JsonIgnore
	public Set<Comment> getComments() {
		return comments;
	}

	@JsonIgnore
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@JsonIgnore
	public Set<Reply> getReplies() {
		return replies;
	}

	@JsonIgnore
	public void setReplies(Set<Reply> replies) {
		this.replies = replies;
	}

	@JsonIgnore
	public Set<Post> getPosts() {
		return posts;
	}

	@JsonIgnore
	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	@JsonIgnore
	public Set<Message> getMessages() {
		return messages;
	}

	@JsonIgnore
	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@JsonIgnore
	public Set<Grupa> getGrupe() {
		return grupe;
	}

	public void setGrupe(Set<Grupa> grupe) {
		this.grupe = grupe;
	}

	public Set<DM> getDms() {
		return dms;
	}

	public void setDms(Set<DM> dms) {
		this.dms = dms;
	}

}
