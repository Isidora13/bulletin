package vp.spring.rcs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class DM {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String creator;
	private Date date;

	@Lob
	@Column(name = "text", length = 500)
	private String text;

	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser receiver;

	public DM() {
		super();
	}

	public DM(Long id, String creator, Date date, String text, SecurityUser receiver) {
		super();
		this.id = id;
		this.creator = creator;
		this.date = date;
		this.text = text;
		this.receiver = receiver;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@JsonIgnore
	public SecurityUser getReceiver() {
		return receiver;
	}

	@JsonIgnore
	public void setReceiver(SecurityUser receiver) {
		this.receiver = receiver;
	}

}
