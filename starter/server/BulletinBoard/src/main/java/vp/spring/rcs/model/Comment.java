package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	SecurityUser creator;

	@Lob 
	@Column(name="text", length=500)
	private String text;
	
	private Date date;

	@ManyToOne(fetch = FetchType.EAGER)
	Post post;

	@OneToMany(mappedBy = "comment", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Set<Reply> replies = new HashSet<Reply>();

	public Comment() {
		super();
	}



	public Comment(Long id, SecurityUser creator, String text, Date date, Post post, Set<Reply> replies) {
		super();
		this.id = id;
		this.creator = creator;
		this.text = text;
		this.date = date;
		this.post = post;
		this.replies = replies;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public SecurityUser getCreator() {
		return creator;
	}

	public void setCreator(SecurityUser creator) {
		this.creator = creator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@JsonIgnore
	public Post getPost() {
		return post;
	}
	@JsonIgnore
	public void setPost(Post post) {
		this.post = post;
	}

	public Set<Reply> getReplies() {
		return replies;
	}

	public void setReplies(Set<Reply> replies) {
		this.replies = replies;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}
	
	

}
