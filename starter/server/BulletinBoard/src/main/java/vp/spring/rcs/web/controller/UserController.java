package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.data.SecurityAuthorityRepository;
import vp.spring.rcs.data.SecurityUserAuthorityRepository;
import vp.spring.rcs.data.UserRepository;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.model.user.SecurityUserAuthority;
import vp.spring.rcs.security.TokenUtils;
import vp.spring.rcs.web.dto.LoginDTO;
import vp.spring.rcs.web.dto.RegisterDTO;
import vp.spring.rcs.web.dto.TokenDTO;

@RestController
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	TokenUtils tokenUtils;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	SecurityAuthorityRepository securityAuthorityRepository;

	@Autowired
	private SecurityUserAuthorityRepository securityUserAuthorityRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	
	
	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO loginDTO) {
        try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
            String genToken = tokenUtils.generateToken(details);
            return new ResponseEntity<TokenDTO>(new TokenDTO(genToken), 
            		HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<TokenDTO>(new TokenDTO(""), HttpStatus.BAD_REQUEST);
        }
	}
	
	@RequestMapping(value="/api/users", method = RequestMethod.GET)
	public ResponseEntity<Page<SecurityUser>> getAllPage(Pageable page) {
			
		
		Page <SecurityUser> userPage = userRepository.findAll(page);
		
		List<SecurityUser> users = userPage.getContent();
		
		Page<SecurityUser> retVal = 
				new PageImpl<>(users, page, userPage.getTotalElements());
		
		return new ResponseEntity<>(retVal, HttpStatus.OK); 
	}
	
	@GetMapping(value="/api/users/all")
	public ResponseEntity <List<SecurityUser>> getAllUsers() {
		List<SecurityUser> useri = userRepository.findAll();
		return new ResponseEntity<>(useri, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/users/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		userRepository.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/api/user")
	public ResponseEntity<SecurityUser> getUser(@RequestParam String username) {
		SecurityUser user = new SecurityUser();
		user = userRepository.findByUsername(username);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}



	@PostMapping(value = "/api/users")
	public ResponseEntity<SecurityUser> addUser(@RequestBody RegisterDTO securityUser) {
		SecurityUser user = new SecurityUser();
		user.setFirstName(securityUser.getFirstName());
		user.setLastName(securityUser.getLastName());
		user.setUsername(securityUser.getUsername());
		user.setPassword(passwordEncoder.encode(securityUser.getPassword()));

		save(user, securityUser.getAuthorities());

		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	


public void save(SecurityUser user, List<String> authorities) {
	final SecurityUser savedUser = userRepository.save(user);

	Set<SecurityUserAuthority> userAuthorities = securityAuthorityRepository.findByNameIn(authorities).stream() // Stream<SecurityAuthority>
			.map(authority -> {
				SecurityUserAuthority userAuthority = new SecurityUserAuthority();

				userAuthority.setAuthority(authority);
				userAuthority.setUser(savedUser);

				return userAuthority;
			}) // Stream<SecurityUserAuthority>
			.collect(Collectors.toSet());

	securityUserAuthorityRepository.save(userAuthorities);
}
}
