package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Post;

public interface PostRepository extends JpaRepository <Post, Long> {

}
