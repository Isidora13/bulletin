package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Comment;

public interface CommentRepository extends JpaRepository <Comment, Long> {
	
	public Comment findByText(String text);
}
