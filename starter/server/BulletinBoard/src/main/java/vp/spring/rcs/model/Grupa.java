package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Grupa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@OneToMany(mappedBy = "grupa", cascade = CascadeType.REFRESH)
	private Set<Message> messages = new HashSet<>();

	@ManyToMany(mappedBy = "grupe", cascade = CascadeType.REFRESH)
	private Set<SecurityUser> creators = new HashSet<>();

	public Grupa() {
		super();
	}

	public Grupa(Long id, String name, Set<Message> messages, Set<SecurityUser> creators) {
		super();
		this.id = id;
		this.name = name;
		this.messages = messages;
		this.creators = creators;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	public Set<SecurityUser> getCreators() {
		return creators;
	}

	public void setCreators(Set<SecurityUser> creators) {
		this.creators = creators;
	}

}
