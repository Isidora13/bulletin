package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.Category;

public interface CategoryRepository extends JpaRepository <Category, Long>{

	public Category findByNameLike (String categoryName);
}
