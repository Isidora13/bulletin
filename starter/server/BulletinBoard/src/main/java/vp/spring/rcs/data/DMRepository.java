package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.rcs.model.DM;

public interface DMRepository extends JpaRepository <DM, Long> {

}
