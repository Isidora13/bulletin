package vp.spring.rcs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Date date;

	@Lob
	@Column(name = "text", length = 500)
	private String text;

	@ManyToOne(fetch = FetchType.EAGER)
	private SecurityUser creator;

	@ManyToOne(fetch = FetchType.EAGER)
	private Grupa grupa;

	public Message() {
		super();
	}

	public Message(Long id, Date date, String text, SecurityUser creator, Grupa grupa) {
		super();
		this.id = id;
		this.date = date;
		this.text = text;
		this.creator = creator;
		this.grupa = grupa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public SecurityUser getCreator() {
		return creator;
	}

	public void setCreator(SecurityUser creator) {
		this.creator = creator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	@JsonIgnore
	public Grupa getGrupa() {
		return grupa;
	}
	@JsonIgnore
	public void setGrupa(Grupa grupa) {
		this.grupa = grupa;
	}

}
