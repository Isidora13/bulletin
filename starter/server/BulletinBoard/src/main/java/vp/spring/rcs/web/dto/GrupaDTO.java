package vp.spring.rcs.web.dto;

import java.util.ArrayList;
import java.util.List;

import vp.spring.rcs.model.Grupa;
import vp.spring.rcs.model.user.SecurityUser;

public class GrupaDTO {
	private Long id;
	private String name;
	private List<MessageDTO> messages = new ArrayList<MessageDTO>();
	private List<SecurityUser> creators = new ArrayList<SecurityUser>();

	public GrupaDTO() {
		super();
	}

	public GrupaDTO(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public GrupaDTO(Grupa grupa) {
		this.id = grupa.getId();
		this.name = grupa.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MessageDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageDTO> messages) {
		this.messages = messages;
	}

	public List<SecurityUser> getCreators() {
		return creators;
	}

	public void setCreators(List<SecurityUser> creators) {
		this.creators = creators;
	}

}
