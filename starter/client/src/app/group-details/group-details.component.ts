import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';
import {
  PostService
} from '../main/post.service';
import {
  Grupa,
  User,
  Massage
} from '../common.models';
import {
  HttpClient
} from '@angular/common/http';
import {
  DatePipe
} from '@angular/common';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.css'],
  providers: [DatePipe]
})
export class GroupDetailsComponent implements OnInit {

  name: string;
  grupa: Grupa;
  messages: Massage[] = [];
  newMessage: Massage;
  creators: User[] = [];
  date = new Date();

  constructor(private route: ActivatedRoute, private postService: PostService, private http: HttpClient,private datePipe: DatePipe) {}

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.name = params['name'];

      this.postService.getGroupByName(this.name).subscribe(
        (response: Grupa) => {
          this.grupa = response;
          this.creators = this.grupa.creators;
          this.messages = this.grupa.messages;
          this.sortByDueDate();
        });
    });
    this.newMessage = new Massage({
      text: "",
      grupa: this.grupa,
      creator: new User({
        username: "petar"
      }),
    })
  }

  saveMessage() {
    this.newMessage.grupa = this.grupa;
    this.newMessage.date = this.date;
    this.postService.postMessage(this.newMessage).subscribe();
    this.refresh();
  }

  refresh(): void {
    window.location.reload();
  }

  sortByDueDate(): void {
    this.datePipe.transform(this.date, 'yyyy-MM-dd');
    this.messages.sort((a: Massage, b: Massage) => {
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
  }
}
