import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Post, Category, User, Grupa } from '../common.models';
import { Router } from '@angular/router';
import { AuthenticationService } from '../security/authentication.service';
import { HttpClient } from '@angular/common/http';
import { PostService } from '../main/post.service';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  public page: any = [];
  public selectedUsers: User[] = [];
  public pageNumber: number;
  public newGrupa: Grupa;
  public allGroups: Grupa[] = [];
  public userGroups: Grupa[] = [];
  public currentUser: any;
  public bula: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private http: HttpClient,
    private postService: PostService) {
  }

  ngOnInit() {
    this.getAllGroups();
    this.getPage(0);
    this.getCurrentUser();
    this.newGrupa = new Grupa({
      name: "",
      creators: []
    })
  }


  getPage(pageNumber: number) {
    this.http.get(`/api/users?size=10&page=${pageNumber}`)
      .subscribe((data: User[]) => {
        this.page = data;
      });
  }

  addToGroup(user: User) {
    if (!this.selectedUsers.includes(user)) {
      this.selectedUsers.push(user);

    }
  }

  createGroup() {
    let grupica = new Grupa({
      name: this.newGrupa.name,
      creators: this.selectedUsers

    })
    this.postService.createNewGroup(grupica).subscribe(
      (response: Grupa) => {
        this.newGrupa = response;
      }
    );
    this.refresh();
  }

  refresh(): void {
    window.location.reload();
  }

  getAllGroups() {
    this.postService.getAllGroups().subscribe(
      (res: Grupa[]) => {
        this.allGroups = res;
        for (let grupa of this.allGroups) {
          for (let creator of grupa.creators) {
            if (creator.username === this.currentUser.username) {
              this.userGroups.push(grupa);

            }
          }
        }
      }
    )

  }

  getCurrentUser() {
    this.currentUser = this.authenticationService.getCurrentUser()
  }

}
