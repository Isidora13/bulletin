import { Component, OnInit } from '@angular/core';
import { Post, Komentar, User, Reply } from '../common.models';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../main/post.service';
import { AuthenticationService } from '../security/authentication.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css'],
  providers: [DatePipe]
})
export class PostDetailsComponent implements OnInit {


  post: Post;
  id: number;
  isDataAvailable: boolean;
  comments: Komentar[];
  newComment:Komentar;
  currentUser:any;
  creator:User;
  date = new Date();
  nebuloza:boolean=false;
  newReply:Reply;
  commentId:number;

  constructor(private route: ActivatedRoute, private postService: PostService,
    private authenticationService: AuthenticationService, private datePipe: DatePipe) {

      this.datePipe.transform(this.date, 'yyyy-MM-dd');
    }


  ngOnInit() {
    this.route.params.subscribe (params => {
      this.isDataAvailable = false;
      this.id = +params ['id'];
      this.postService.getPost(this.id).subscribe (
        (response: Post)=> {
          this.post = response;
          this.isDataAvailable = true;
        });
    });

    this.currentUser = this.authenticationService.getCurrentUser();
        this.getComments();
        this.newComment = new Komentar({
          creator :this.currentUser,
          text:"",
          date: new Date
        });



  }

  getComments() {
    this.postService.getAllComments(this.id).subscribe(
      (res: Komentar[]) => {
        this.comments = res;
      }
    )
  }

  saveComment(){
    this.postService.postNewComment(this.id,this.newComment).subscribe(() => {this.getComments()});
    this.refresh();
    this.newComment = new Komentar({
      text:""
    });
  }

  refresh(): void {
    window.location.reload();
}

public sortByDueDate(): void {
  this.comments.sort((a: Komentar, b: Komentar) => {
    return new Date(b.date).getTime() - new Date(a.date).getTime();

  });
}

createReply(id:number) {
  this.nebuloza=true;

  this.newReply = new Reply ({
    text:"",
    creator :this.currentUser,
    })
    this.commentId=id;
}

saveReply(){
  this.postService.postReply(this.newReply, this.commentId).subscribe(
    (response: Reply) => {
      this.newReply = response;
    }
  )
  this.refresh();
}

}
