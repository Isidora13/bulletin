import { Component, OnInit } from '@angular/core';
import { Post, User } from '../common.models';
import { PostService } from '../main/post.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public posts:Post[];
  public newPost:Post;
  public users:User[];

  constructor(private postService:PostService) {

  }

  ngOnInit() {

    this.loadData();
  }

  private loadData(){
    this.postService.
      getPosts().
        subscribe((posts: Post[]) => {this.posts = posts;});
  }

  delete(id: number){
    this.postService.deletePost(id).subscribe(
      () => {
        this.loadData();
      }
    );
  }



}
