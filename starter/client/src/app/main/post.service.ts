import { Injectable } from '@angular/core';

import { Post, Komentar, User, Reply, Category, Grupa, Massage, DM } from '../common.models';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
		return this.http.get<Post[]>("/api/posts");
	}

	getPost(id: number): Observable<Post> {
		return this.http.get<Post>(`/api/posts/${id}`);
  }

  createPost(newPost:Post): Observable<Post> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Post> ("/api/posts", newPost, { headers })
  }

  deletePost(id: number): Observable<Post> {
		return this.http.delete<Post>(`/api/posts/${id}`);
  }

  getAllUsers() :Observable<User[]> {
    return this.http.get<User[]>("/api/users");
  }

  getAllUsersNormal() :Observable<User[]> {
    return this.http.get<User[]>("/api/users/all");
  }

  getAllComments(id:number): Observable<Komentar[]> {
    return this.http.get<Komentar[]>("/api/posts/"+id+"/comments")
  }

  postNewComment(id:number, newComment:Komentar):Observable<Komentar>{
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Komentar>("/api/posts/"+id+"/comments", newComment, { headers });
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]> ("/api/categories");
  }

  deleteUser(id:number): Observable<User> {
    return this.http.delete<User>(`/api/users/${id}`);
  }

  addNewUser(newUser:User): Observable<User>{
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<User>(`/api/users`, newUser, {headers});
  }

  createNewGroup(newGrupa:Grupa): Observable<Grupa> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Grupa> ("/api/group", newGrupa, {headers} )
  }

  getGroupByName (name:string): Observable<Grupa> {
    return this.http.get<Grupa>("/api/group?name="+name)
  }

  getAllGroups (): Observable<Grupa[]> {
    return this.http.get<Grupa[]>("/api/groups");
  }

  postReply (newReply:Reply, commentId:number) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Reply>("/api/reply?commentId="+commentId, newReply, { headers });
  }

  postMessage(newMessage:Massage) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Massage>("/api/group/message", newMessage, {headers});
  }

  getUser(username:string): Observable<User> {
    return this.http.get<User>("/api/user?username="+username);
  }

  createNewDM(newDM:DM, receiver:string):Observable<DM> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<DM>("api/dm?username="+receiver, newDM, {headers});
  }

}
