
export class Post implements PostInterface {

  public id?:number;
  public text?:string;
  public title?:string;
  public date?:Date;
  public creator?:User;
  public category?:Category;
  public comments?:Komentar[];

  constructor(postCfg:PostInterface)
  {
    this.id=postCfg.id;
    this.text=postCfg.text;
    this.title = postCfg.title;
    this.date=postCfg.date;
    this.creator=postCfg.creator;
    this.category=postCfg.category;
    this.comments=postCfg.comments;
  }
}

export class Komentar implements KomentarInterface{
  public text?:string;
  public replies?:Reply[];
  public creator?:User;
  public id?:number;
  public date?:Date;
  constructor(komentarCfg:KomentarInterface){
    this.id=komentarCfg.id;
    this.replies=komentarCfg.replies;
    this.creator=komentarCfg.creator;
    this.text=komentarCfg.text;
    this.date = komentarCfg.date;
  }
}

export class User implements UserInterface{
  public id?:number;
  public username?:string;
  public password?:string;
  public firstName?:string;
  public lastName?:string;
  public authorities?:string[];
  public dms?:DM[];
  constructor(userCfg:UserInterface){
    this.id=userCfg.id;
    this.username=userCfg.username;
    this.password=userCfg.password;
    this.firstName=userCfg.username;
    this.lastName=userCfg.lastName;
    this.authorities=userCfg.authorities;
    this.dms=userCfg.dms;
  }
}

export class Category implements CategoryInterface{

  public id?:number;
  public name?:string

  constructor (c:CategoryInterface) {
    this.id = c.id;
    this.name = c.name;
  }


}

export class Reply implements ReplyInterface{
  public id?:number;
  public creator?:User;
  public text?:string;
  public comment?:Komentar;
  constructor (r:ReplyInterface) {
    this.id = r.id;
    this.creator = r.creator;
    this.text = r.text;
    this.comment=r.comment;
  }
}

export class Grupa implements GroupInterfae {
  public id?:number;
  public creators?:User[];
  public messages?:Massage[];
  public name?:string;
  constructor(g:GroupInterfae) {
    this.id=g.id;
    this.creators=g.creators;
    this.messages=g.messages;
    this.name=g.name;
  }
}

export class Massage implements MessageInterface {
  public id?:number;
  public creator?:User;
  public text?:string;
  public grupa?:Grupa;
  public date?:Date;
  constructor(m:MessageInterface) {
    this.id=m.id;
    this.creator=m.creator;
    this.text=m.text;
    this.grupa=m.grupa;
    this.date=m.date;
  }
}

export class DM implements DmInterface {
  public id?:number;
  public text?:string;
  public date?:Date;
  public creator?:string;
  public receiver?:User;
  constructor(d:DmInterface) {
    this.id=d.id;
    this.text=d.text;
    this.date=d.date;
    this.creator=d.creator;
    this.receiver=d.receiver;
  }
}

interface DmInterface{
  id?:number,
  text?:string,
  date?:Date,
  creator?:string,
  receiver?:User
}

interface PostInterface {

  id?:number,
   text?:string,
   title?:string,
   date?:Date,
   creator?:User,
   category?:Category,
   comments?:Komentar[]

}

interface UserInterface {
  id?:number,
  username?:string,
  password?:string,
  firstName?:string,
  lastName?:string,
  authorities?:string[],
  dms?:DM[]
}

interface CategoryInterface {
  id?:number,
  name?:string,
}

interface KomentarInterface {
  id?:number,
  creator?:User,
  text?:string,
  replies?:Reply[],
  date?:Date
}

interface ReplyInterface {
  id?:number,
  creator?:User,
  text?:string,
  comment?:Komentar
}

interface GroupInterfae {
  id?:number,
  creators?:User[],
  messages?:Massage[],
  name?:string
}

interface MessageInterface {
  id?:number,
  text?:string,
  grupa?:Grupa,
  creator?:User,
  date?:Date
}
