import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PostService } from '../main/post.service';
import { AuthenticationService } from '../security/authentication.service';
import { User, Post } from '../common.models';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users:User[];
  posts:Post[];
  newUser:User;

  page: any;
  pageNumber:number;

  @Output() deleteUser: EventEmitter<number> = new EventEmitter();

  constructor(
    private postService:PostService,
    private router: Router,
    private http: HttpClient,
    private authenticationService: AuthenticationService
    ) { }

  ngOnInit() {
    this.newUser= new User ({
      username:"",
      password:"",
      firstName:"",
      lastName:"",
      authorities:["ROLE_USER"]
    })
    this.getPage(0);
    this.pageNumber=0;
  }

  delete(id:number){
    this.postService.deleteUser(id).subscribe();
    this.refresh();
  }

  details(id:number) {
    this.router.navigate(['/user/'+id]);
  }

  refresh(): void {
    window.location.reload();
}

getPage(pageNumber: number) {
  this.http.get(`/api/users?size=3&page=${pageNumber}`)
    .subscribe(data => {
      this.page = data;
    });
}

isAdmin(): boolean {
  return this.authenticationService.isAdmin();
}

changePage(i: number) {
  this.pageNumber += i;
  this.getPage(this.pageNumber);
}

  postTable(){
    this.router.navigate(['main']);
  }
  userTable(){
    this.router.navigate(['users']);
  }

  addUser() {
    this.postService.addNewUser(this.newUser).subscribe();
    this.refresh();
  }
}
