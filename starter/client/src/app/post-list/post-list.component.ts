import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Post,
  Category,
  User
} from '../common.models';
import {
  Router
} from '@angular/router';
import {
  AuthenticationService
} from '../security/authentication.service';
import {
  HttpClient
} from '@angular/common/http';
import {
  PostService
} from '../main/post.service';
import {
  DatePipe
} from '@angular/common';
import * as _ from 'lodash';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  providers: [DatePipe]
})
export class PostListComponent implements OnInit {

  newPost: Post;
  categories: Category[];
  currentUser: any;
  date = new Date();
  // displayPosts: Post[];


  @Output() deletePost: EventEmitter < number > = new EventEmitter();

  page: any;
  pageNumber: number;
  posts: Post[];
  users: User[];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private http: HttpClient,
    private postService: PostService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.currentUser = this.authenticationService.getCurrentUser();
    this.loadData();
    this.getPage(0);
    this.pageNumber = 0;
    this.getCategories();
    this.newPost = new Post({
      title: "",
      text: "",
      date: new Date(),
      creator: this.currentUser,
      category: new Category({})
    });
  }

  loadData() {
    this.postService.
    getPosts().
    subscribe((posts: Post[]) => {
      this.posts = posts;
      this.sortByDueDate();
    });
    this.postService.
    getAllUsers().
    subscribe((users: User[]) => {
      this.users = users
    });
  }

  sortByDueDate(): void {
    this.datePipe.transform(this.date, 'yyyy-MM-dd');
    this.posts.sort((a: Post, b: Post) => {
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
  }

  delete(id: number) {
    this.postService.deletePost(id).subscribe()
    this.refresh();
  }

  refresh(): void {
    window.location.reload();
  }

  isAdmin(): boolean {
    return this.authenticationService.isAdmin();
  }

  getPage(pageNumber: number) {
    this.http.get(`/api/posts/list?size=3&page=${pageNumber}`)
      .subscribe(data => {
        this.page = data;
      });
  }

  changePage(i: number) {
    this.pageNumber += i;
    this.getPage(this.pageNumber);
  }

  details(id: number) {
    this.router.navigate(['/post/' + id]);
  }

  postTable() {
    this.router.navigate(['main']);
  }
  userTable() {
    this.router.navigate(['users']);
  }

  getCategories() {
    this.postService.getCategories().subscribe(
      (response: Category[]) => {
        this.categories = response;
      }
    )
  }

  createPost() {
    this.postService.createPost(this.newPost).subscribe(
      (response: Post) => {
        this.newPost = response;
        this.refresh();
      }
    )
  }



}
