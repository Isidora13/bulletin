import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateAuthGuard } from './security/can-activate-auth.guard';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { MainComponent } from './main/main.component';
import { PostListComponent } from './post-list/post-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { GroupComponent } from './group/group.component';
import { GroupDetailsComponent } from './group-details/group-details.component';
import { DmComponent } from './dm/dm.component';

const routes: Routes = [
  { path: 'post/:id', component: PostDetailsComponent, canActivate:[CanActivateAuthGuard] },
  { path: 'main', component: PostListComponent, canActivate:[CanActivateAuthGuard] },
  { path: 'group', component:GroupComponent},
  { path: 'group/:name', component: GroupDetailsComponent},
  { path: 'dm', component:DmComponent},
  { path: 'users', component: UserListComponent},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
