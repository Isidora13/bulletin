import { Component, OnInit } from '@angular/core';
import { User, DM } from '../common.models';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../main/post.service';
import { AuthenticationService } from '../security/authentication.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dm',
  templateUrl: './dm.component.html',
  styleUrls: ['./dm.component.css'],
  providers: [DatePipe]
})
export class DmComponent implements OnInit {

  id:number;
  user:User;
  dms:DM[]=[];
  currentUser:any;
  newDM:DM;
  allUsers:User[]=[];
  receiver:string;
  date:Date;

  constructor(private route: ActivatedRoute, private postService: PostService,
    private authenticationService: AuthenticationService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.getCurrentUser();
    this.getAllUsers();
    this.getUser();
    this.newDM = new DM ({
      date: new Date,
      text: "",
      creator:this.currentUser.username
    });
    this.receiver;
  }

  getCurrentUser() {
    this.currentUser = this.authenticationService.getCurrentUser()
  }

  getUser() {
    this.postService.getUser(this.currentUser.username).subscribe(
      (res:User) => {
        this.user=res;
        this.dms=this.user.dms;
        this.sortByDueDate();
      })
  }

  getAllUsers(){
    this.postService.getAllUsersNormal().subscribe(
      (res:User[])=>{
        this.allUsers=res;
      }
    )
  }

  createNewDm() {
    this.postService.createNewDM(this.newDM, this.receiver).subscribe();
    this.refresh();
  }

  refresh(): void {
    window.location.reload();
  }

  sortByDueDate(): void {
    this.datePipe.transform(this.date, 'yyyy-MM-dd');
    this.dms.sort((a: DM, b: DM) => {
      return new Date(b.date).getTime() - new Date(a.date).getTime();
    });
  }

}
